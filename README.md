# Shakespeare

## Installation
1. Install theme
2. Create your sub theme based on `shakespeare`
3. Copy the `regions` section from [shakespeare.info.yml](shakespeare.info.yml) into your sub themes `info.yml` file
    * Check [drupal.org](https://www.drupal.org/docs/8/theming-drupal-8/creating-a-drupal-8-sub-theme-or-sub-theme-of-sub-theme) for additional information
